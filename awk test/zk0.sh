#!/bin/sh

export MYID=1
SERVERS='localhost,localhost,localhost'


#printf '%s' "$SERVERS" | awk 'BEGIN { RS = "," }; { printf "server.%i=%s:2888:3888\n", NR, $0 }' >> ./zoo.cfg
#printf '%s' "$SERVERS" | awk 'BEGIN { RS = "," }; { printf "server.%i=%s:2888%s:3888%s\n", NR, $0, NR, NR }' >> ./zoo.cfg
printf '%s' "$SERVERS" | awk 'BEGIN {id=ENVIRON["MYID"] ;  RS = "," }; { printf "server.%i=%s:2888%i:3888%i\n", NR, $0, id, id }' >> ./zoo.cfg
